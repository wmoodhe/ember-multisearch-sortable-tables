import Component from '@ember/component';
import { getOwner } from '@ember/application';

export default Component.extend({
  routeName: '',

  didInsertElement() {
    this._super(...arguments);
    this.set('routeName', getOwner(this).lookup('controller:application').currentPath);
  },

  actions: {
    close() {
      this.close();
    },

    delete(id) {
      this.delete(id);
    },

    rename(id) {
      this.rename(id);
    }
  }
});
