import Component from '@ember/component';

export default Component.extend({
  tagName: '',
  actions: {
    check(){
      this.set('item.checked', !this.get('item.checked'));
    }
  }
});
