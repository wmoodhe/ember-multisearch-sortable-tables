import Component from '@ember/component';

export default Component.extend({
  initialName: '',
  nameMaxLength: 100,

  didUpdateAttrs() {
    this._super(...arguments);

    if (this.get('savedSearch')) {
      this.set('initialName', this.get('savedSearch.name'));
    } else {
      this.set('initialName', '');
    }
  },

  didDestroyElement() {
    this._super(...arguments);
    this.set('savedSearch', null);
    this.set('error', null);
  },

  actions: {
    cancel() {
      this.set('savedSearch.name', this.get('initialName'));
      this.set('error', null);
      this.cancel();
    },

    save() {
      let name = this.get('savedSearch.name');

      if (!name || name.length === 0 || name.length === 1 && name === ' ') {
        this.set('error', 'Name cannot be blank.');

        return;
      }

      if (name.length > this.get('nameMaxLength')) {
        this.set('error', 'Name cannot exceed ' + this.get('nameMaxLength') + ' characters.');

        return;
      }

      var i, savedSearches, hasError = false;

      savedSearches = this.get('savedSearches');

      for (i = 0; i < savedSearches.get('length'); i++) {
        let savedSearch = savedSearches.objectAt(i);

        if (savedSearch.get('id') === this.get('savedSearch.id')) {
          continue;
        }

        if (savedSearch.get('name') === this.get('savedSearch.name')) {
          this.set('error', 'This name has already been taken.');
          hasError = true;

          break;
        }
      }

      if (hasError) {
        return;
      }

      this.set('error', null);

      this.save();
    }
  }
});
