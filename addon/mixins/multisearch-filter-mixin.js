import { A } from '@ember/array';
import Mixin from '@ember/object/mixin';
import FilterObject from '../utils/filter-object';

export default Mixin.create({
  currentSize: 0,
  totalSize: 0,
  queryParamsLoaded: false,

  setSearchFilters(searchFilters) {
    var filters = A();

    searchFilters.forEach(searchFilter => {
      var filter = FilterObject.create();
      filter.loadFilter(searchFilter);
      filters.addObject(filter);
    });

    this.set('searchFilters', filters);
  },

  setSearchFilterContentByQueryParam(targetQueryParam, content) {
    let filters = this.get('searchFilters');

    filters.forEach(f => {
      let queryParam = f.get('queryParam');

      if (queryParam && queryParam === targetQueryParam) {
        f.set('content', content);
      }
    });
  },

  actions: {
    setFilteredObjects: function(filteredObjects){
      this.set(this.get('filterObjectsTarget'), filteredObjects);
    },

    setItemCounts: function(currentSize, totalSize) {
      this.set('currentSize', currentSize);
      this.set('totalSize', totalSize);
    },

    setQueryParam: function(paramName, paramValue) {
      this.set(paramName, paramValue);
    },

    setQueryParams: function(params) {
      var i, keys, shouldReload = false;

      keys = Object.keys(params);

      for (i = 0; i < keys.length; i++) {
        let key = keys[i];

        if (this.get(key) !== params[key]) {
          shouldReload = true;
        }
      }

      if (shouldReload && this.get('queryParamsLoaded')) {
        this.set('queryParamsLoaded', false);
        this.setProperties(params);
        this.send('reload');
      }
    },

    retrieveQueryParams: function() {
      this.get('searchFilters').forEach(filter => {
        let queryParam = filter.get('queryParam');

        if (queryParam) {
          let queryValue = this.get(queryParam);

          if (queryValue) {
            filter.setValue(queryValue);
          }
        }
      });

      this.set('queryParamsLoaded', true);
    }
  }
});
