import Mixin from '@ember/object/mixin';

export default Mixin.create({
  sortEventEnabled: false,
  actions: {
    sortColumns(targets) {
      for (var i = 0; i < targets.length; i++) {
        this.send('sortByColumn', targets[i]);
      }
    },

    sortByColumn(target) {
      var reverseTarget;

      if (target.indexOf(':desc') >= 0) {
        reverseTarget = target.substring(0, target.length - 5);
      } else {
        reverseTarget = target + ':desc';
      }

      if (Array.isArray(this.get('sortBy'))) {
        if (this.get('sortBy').includes(reverseTarget)) {
          this.get('sortBy').removeObject(reverseTarget);
        }

        if (!this.get('sortBy').includes(target)) {
          this.get('sortBy').unshiftObject(target);
        }
      } else {
        this.set('sortBy', target);
      }

      this.notifyPropertyChange('sortBy');
    }
  }
});
